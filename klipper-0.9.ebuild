# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils desktop

DESCRIPTION="Klipper split out from plasma-workspace"
HOMEPAGE="https://github.com/yshui/klipper"
SRC_URI="https://gitlab.com/Butthurt/${PN}/-/archive/${PV}/${P}.tar.bz2"
KEYWORDS="~amd64 ~arm ~x86"

LICENSE="GPL-3"
SLOT="5"

BDEPEND="
	kde-frameworks/kdelibs4support
	kde-frameworks/prison
	!kde-plasma/plasma-workspace
"

DEPEND="${BDEPEND}"

src_install() {
	cmake-utils_src_install
	newicon 128-klipper.png ${PN}.png
}
